
module "install" {
  source     = "../../modules/wrapper"
  k8s_subnet = "192.168.192.0/18"
}

output "helm_release" {
  value = module.install.release
}
