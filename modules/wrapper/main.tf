
module "metallb" {
  source = "git::https://gitlab.com/nolte-collection-v2/modules/tf-modules/tf-k8s-metallb.git//modules/install"
  #source          = "/tf-modules/tf-k8s-metallb/modules/install"
  layer2_ip_range = "${cidrhost(var.k8s_subnet, var.number_first_host)}-${cidrhost(var.k8s_subnet, var.number_of_hosts)}"
}

locals {
  ingress_extrenal_ip = cidrhost(var.k8s_subnet, var.number_first_host + 1)
  EXTRA_VALUES = {
    controller = {
      #admissionWebhooks = {
      #  patch = {
      #    image = {
      #      tag = "arm-v1.3.0"
      #    }
      #  }
      #}
      service = {
        externalIPs = [local.ingress_extrenal_ip]
        annotations = {
          "metallb.universe.tf/address-pool" = module.metallb.address_pool_name
        }
      }
    }
  }
}

resource "kubernetes_namespace" "ngix_namespace" {
  metadata {
    name = "nginx-ingress"
  }
}

module "ingress" {
  depends_on = [module.metallb]
  source     = "git::https://gitlab.com/nolte-collection-v2/modules/tf-modules/tf-k8s-nginx-ingress.git//modules/install"
  #source       = "/tf-modules/tf-k8s-nginx-ingress/modules/install"
  extra_values = local.EXTRA_VALUES
  namespace    = kubernetes_namespace.ngix_namespace.metadata[0].name
}

output "ingress_extrenal_ip" {
  value = local.ingress_extrenal_ip
}
output "address_pool_name" {
  value = module.metallb.address_pool_name
}
