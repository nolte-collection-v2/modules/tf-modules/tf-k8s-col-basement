variable "k8s_subnet" {
  default = "192.168.192.0/18"
}
variable "number_first_host" {
  default = "1"
}

variable "number_of_hosts" {
  default = 16380
}
